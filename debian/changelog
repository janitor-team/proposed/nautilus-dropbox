nautilus-dropbox (2019.02.14-1) unstable; urgency=medium

  * New upstream release. (Closes: #921222)
    - Port to Python 3 (Closes: #937113)
    - Drop patches applied upstream, refresh remaining patches.
  * d/compat, d/control: Replace d/compat with debhelper-compat, bump to 12.
  * d/control:
    - Add build-dep on dh-python, replace python2 depends with python3.
      + python → python3-all
      + python-docutils → python3-docutils
      + python-gtk2 → python3-gi, gir1.2-gdkpixbuf-2.0, gir1.2-gtk-3.0.
        Closes: #885355
      + ${python:Depends} → ${python3:Depends}
      + python-gpgme → python3-gpg (Closes: #876985)
    - Drop libappindicator1 from Recommends.
    - Drop policykit-1 from Depends.
    - Update Vcs-* fields.
    - Set R³ to no.
  * d/rules: Drop explicit --with autoreconf, replace python2 with python3.
  * d/s/lintian-overrides, d/s/options: Drop, no longer needed.
  * Update Standards-Version to 4.4.0.

 -- Unit 193 <unit193@ubuntu.com>  Mon, 09 Sep 2019 18:38:58 -0400

nautilus-dropbox (2015.10.28-1) unstable; urgency=medium

  * [788b88a] Imported Upstream version 2015.10.28

 -- Luke Faraone <lfaraone@debian.org>  Sat, 24 Sep 2016 17:38:46 +0000

nautilus-dropbox (2.10.0-3) unstable; urgency=medium

  [ Jeremy Bicha ]
  * [8fdd287] Adapt to nautilus multi-archification (Closes: #838747)

  [ Luke W Faraone ]
  * [752fde3] Move Depends: on gpgme to a Recommends: since GPGME isn't needed
    and currently FTBFS.

 -- Luke Faraone <lfaraone@debian.org>  Sat, 24 Sep 2016 17:16:43 +0000

nautilus-dropbox (2.10.0-2) unstable; urgency=medium

  * Upload to unstable.
  * [edeb6a0] Adopt package, thanks hyperair@debian.org for your work over the
    years.
  * [2627836] Update VCS links
  * [cf03e48] Update homepage to point to GitHub
  * [30be68c] Update copyright, fix upstream contacts

 -- Luke Faraone <lfaraone@debian.org>  Sun, 31 Jan 2016 21:00:01 +0000

nautilus-dropbox (2.10.0-1) experimental; urgency=medium

  [ Chow Loong Jin ]
  * [3170728] Imported Upstream version 2.10.0
  * [1775a47] No-change bump of Standards-Version to 3.9.6

  [ mdt ]
  * [208d7a8] wrap-and-sort
  * [d5f0686] Bump debhelper to 9.

 -- Chow Loong Jin <hyperair@debian.org>  Sat, 29 Nov 2014 21:17:33 +0800

nautilus-dropbox (1.6.2-3) unstable; urgency=medium

  * [0491c49] Patch gdk_threads_init crash (Closes: #763724)

 -- Chow Loong Jin <hyperair@debian.org>  Fri, 03 Oct 2014 12:07:58 +0800

nautilus-dropbox (1.6.2-2) unstable; urgency=medium

  [ Unit 193 ]
  * [f412574] Add vrms reasons file. Closes: #708606

  [ Chow Loong Jin ]
  * [dcb4926] Disable global installation of dropbox (Closes: #758037)
  * [a92d214] Refresh non-interactive-update.patch

 -- Chow Loong Jin <hyperair@debian.org>  Sun, 14 Sep 2014 16:36:05 +0800

nautilus-dropbox (1.6.2-1) unstable; urgency=medium

  * [5aa9039] Imported Upstream version 1.6.2
  * [2de0806] Refresh patches

 -- Chow Loong Jin <hyperair@debian.org>  Sat, 03 May 2014 00:53:58 +0800

nautilus-dropbox (1.6.1-1) unstable; urgency=low

  * [2e7146f] Fix missing colour output in dropbox (Closes: #730656)
  * [fba6519] Imported Upstream version 1.6.1
  * [a4e21c3] Drop fix-colour-output.patch (upstreamed)
  * [864fd02] No-change bump of Standards-Version to 3.9.5

 -- Chow Loong Jin <hyperair@debian.org>  Thu, 02 Jan 2014 23:01:48 +0800

nautilus-dropbox (1.6.0-2) unstable; urgency=low

  * [207aa03] Add recommends on libappindicator1.
    Turns out dropbox uses it when it detects an appindicator-based environment.
    Thanks to Mattia Rizzolo <mattia@mapreri.org> (LP: #1242413)

 -- Chow Loong Jin <hyperair@debian.org>  Thu, 07 Nov 2013 12:10:09 +0800

nautilus-dropbox (1.6.0-1) unstable; urgency=low

  * [634af0b] Imported Upstream version 1.6.0
  * [b26e718] Update maintainer field.
    Taken over maintainership of nautilus-dropbox with Raphaël's approval.
  * [d99f903] Update copyright year for Dropbox
  * [14c937a] Document copyright for myself
  * [d288ece] No-change bump of Standards-Version to 3.9.4
  * [3230f36] Redo display-error-string-when-download-failed.patch
    dropbox.in seems to have undergone quite a number of changes, so reimplement
    this feature on top of it.
  * [dc5ac7c] Refresh patches

 -- Chow Loong Jin <hyperair@debian.org>  Sun, 22 Sep 2013 18:23:03 +0800

nautilus-dropbox (1.4.0-3) unstable; urgency=low

  * Add "Conflicts: dropbox" since dropbox (the company) renamed
    their nautilus-dropbox into dropbox, and the same software is
    now packaged under two package names. Closes: #686863

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 07 Sep 2012 08:04:57 +0200

nautilus-dropbox (1.4.0-2) unstable; urgency=low

  * Update add-http-proxy-option.patch and use-pkexec-to-get-root-
    rights.patch to support and handle --https-proxy option too. Dropbox
    now redirects the download URL to an https URL so it might be needed
    to get the download working in some situations.
  * Add display-error-string-when-download-failed.patch to provide more
    information when the download failed. It makes it easier to respond
    to user support requests.

 -- Raphaël Hertzog <hertzog@debian.org>  Sun, 12 Aug 2012 15:40:00 +0200

nautilus-dropbox (1.4.0-1) unstable; urgency=low

  * New upstream release (without any change... just to increase
    the version number above the version that was in Ubuntu LTS).
  * Update Standards-Version to 3.9.3 (no change needed).

 -- Raphaël Hertzog <hertzog@debian.org>  Mon, 09 Jul 2012 09:49:31 +0200

nautilus-dropbox (0.7.1-2) unstable; urgency=low

  * Update watch file to cope with changes on the dropbox website.
  * Update copyright file to fix 2 syntax errors and to add a disclaimer
    concerning the fact that the package is in non-free. Thanks
    to Charles Plessy for the patch. Closes: #650735
  * Split upstreamable changes from use-var-lib-dropbox.patch into
    do-not-hardcode-dropboxd-path.patch.
  * Split non-upstream changes from dropbox-update.patch into
    non-interactive-update.patch.
  * Update use-var-lib-dropbox.patch to ensure that the extracted files are
    owned by root.
  * Stop dropbox in the prerm and not in the postrm since the program
    is no longer available at that point.
  * New patch add-http-proxy-option.patch to force the http_proxy environment
    variable.
  * Update use-pkexec-to-get-root-rights.patch to forward the http_proxy
    environment variable across pkexec. Closes: #651065

 -- Raphaël Hertzog <hertzog@debian.org>  Wed, 28 Dec 2011 11:35:56 +0100

nautilus-dropbox (0.7.1-1) unstable; urgency=low

  * New upstream release.
  * Do not touch /var/lib/update-notifier/dpkg-run-stamp as
    /etc/apt/apt.conf.d/99update-notifier does it for us anyway at the end of
    the APT run. Manual installations of the package will not get the
    message but that's ok.
  * Drop /var/lib/update-notifier/user.d/dropbox-{re,}start-required
    on removal since we create them in the postinst and they
    might still be there if update-notifier is not installed.
    Closes: #648215
  * Drop fix-path-of-desktop-file.patch, applied upstream.
  * Refresh all other patches.

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 22 Nov 2011 11:04:25 +0100

nautilus-dropbox (0.7.0-2) unstable; urgency=low

  * Fix the distribution in the changelog entry for version 0.7.0-1
    (UNRELEASED → unstable).

 -- Raphaël Hertzog <hertzog@debian.org>  Sat, 29 Oct 2011 16:28:59 +0200

nautilus-dropbox (0.7.0-1) unstable; urgency=low

  * New upstream version.
  * Drop use-system-rst2man.patch, merged upstream.
  * Refresh all other patches.
  * This version implements a GPG verification of the tarball
    downloaded from dropbox.com. Closes: #632771
    The wrapper needs python-gpgme to perform this check, so
    add it to the dependencies.
  * Switch to debhelper compatibility level 9 in order to benefit
    from the hardening build flags.
  * Use dh --with=python2 since previous change dropped usage of
    python-support.
  * Use -Wl,--as-needed to drop unnecessary libraries linked into the plugin.
    This requires dh-autoreconf's ability to patch ltmain.sh. Thus add
    it to the Build-Depends field.
  * New patch to remove files that will be overwritten by the unpack
    to avoid failures with ETXTBUSY. LP: #818014
  * New patch to ask for root rights when we have to install dropbox as
    user. LP: #821212
    Add dependency on policykit-1 since the patch makes use of pkexec.
  * Thanks to this, we don't have to ensure that the download succeeds
    in the postinst. LP: #876340
  * Update fix-path-of-desktop-file.patch to be upstreamable.

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 28 Oct 2011 20:32:45 +0200

nautilus-dropbox (0.6.9-1) unstable; urgency=low

  * New upstream version.
  * Uses system's rst2man instead of the embedded one. Avoids a build
    failure once python-docutils 0.8 is uploaded. Thanks to Jakub Wilk.
    Closes: #639030

 -- Raphaël Hertzog <hertzog@debian.org>  Wed, 07 Sep 2011 10:17:02 +0200

nautilus-dropbox (0.6.8-1) unstable; urgency=low

  * New upstream version.
  * Fix the watch file to not hardcode the version.

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 15 Jul 2011 10:51:10 +0200

nautilus-dropbox (0.6.7-3) unstable; urgency=medium

  * Add a Suggests: nautilus.
  * Drop usage of https as urllib2 doesn't seem to correctly use the
    http proxy for https download. Upstream will soon provide GPG
    signatures so that this is not a big deal. Closes: #631300
  * Medium urgency to get the fixed version in testing.

 -- Raphaël Hertzog <hertzog@debian.org>  Wed, 29 Jun 2011 11:34:46 +0200

nautilus-dropbox (0.6.7-2) unstable; urgency=low

  * Add lintian override for the use of killall.
  * Include full license for CC-BY-ND.
  * Switch to HTTPS for the download, but we still have to implement
    HTTPS certificate validation.
  * Add the "XS-Autobuild: yes" header as per
    http://lists.debian.org/debian-devel-announce/2006/11/msg00012.html

 -- Raphaël Hertzog <hertzog@debian.org>  Thu, 09 Jun 2011 09:02:14 +0200

nautilus-dropbox (0.6.7-1) unstable; urgency=low

  * Initial release. Closes: #544499, #613788
  * The package goes in non-free because the pictures are non-free (CC-BY-ND)
    even if the code is GPL. Otherwise it could have gone into contrib since
    it needs the proprietary dropbox binary to work.
  * Build the package only on i386 and amd64 since other architectures are not
    supported by Dropbox.

 -- Raphaël Hertzog <hertzog@debian.org>  Sat, 04 Jun 2011 16:07:54 +0200
